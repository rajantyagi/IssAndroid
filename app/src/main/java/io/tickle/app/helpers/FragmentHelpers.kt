package io.tickle.app.helpers

import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import io.tickle.app.R

/**
 * Created by rajan on 2/16/2018.
 */
class FragmentHelpers {

    companion object {
        fun startFragment(activity: AppCompatActivity?, fragment: Fragment) {
            val transaction = activity?.supportFragmentManager?.beginTransaction()
            transaction?.let {
                transaction.replace(R.id.container, fragment, fragment.javaClass.simpleName)
                transaction.commit()
            }

        }
    }
}