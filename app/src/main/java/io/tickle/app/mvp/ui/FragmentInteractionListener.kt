package io.tickle.app.mvp.ui

/**
 * Created by rajan on 2/16/2018.
 */
interface FragmentInteractionListener {

    enum class FragmentType {BROWSE,SEND,PROFILE}
    fun onFragmentInteraction(type: FragmentType)

}