package io.tickle.app.mvp.ui.tickles

import android.content.Context
import android.graphics.PointF
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.andremion.louvre.Louvre
import com.otaliastudios.cameraview.*
import io.tickle.app.R
import io.tickle.app.mvp.ui.FragmentInteractionListener
import kotlinx.android.synthetic.main.control_panel_layout.*
import kotlinx.android.synthetic.main.fragment_send_tickle.*
import java.io.File


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [SendTickleFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [SendTickleFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SendTickleFragment : Fragment() {

    private var mListener: FragmentInteractionListener? = null

    private var cameraCallback = object : CameraListener() {
        /**
         * Notifies that the camera was opened.
         * The options object collects all supported options by the current camera.
         */
        override fun onCameraOpened(options: CameraOptions?) {}

        /**
         * Notifies that the camera session was closed.
         */
        override fun onCameraClosed() {}

        /**
         * Notifies about an error during the camera setup or configuration.
         * At the moment, errors that are passed here are unrecoverable. When this is called,
         * the camera has been released and is presumably showing a black preview.
         *
         * This is the right moment to show an error dialog to the user.
         */
        override fun onCameraError(error: CameraException) {}

        /**
         * Notifies that a picture previously captured with capturePicture()
         * or captureSnapshot() is ready to be shown or saved.
         *
         * If planning to get a bitmap, you can use CameraUtils.decodeBitmap()
         * to decode the byte array taking care about orientation.
         */
        override fun onPictureTaken(picture: ByteArray?) {}

        /**
         * Notifies that a video capture has just ended. The file parameter is the one that
         * was passed to startCapturingVideo(File), or a fallback video file.
         */
        override fun onVideoTaken(video: File) {}

        /**
         * Notifies that the device was tilted or the window offset changed.
         * The orientation passed can be used to align views (e.g. buttons) to the current
         * camera viewport so they will appear correctly oriented to the user.
         */
        override fun onOrientationChanged(orientation: Int) {}

        /**
         * Notifies that user interacted with the screen and started focus with a gesture,
         * and the autofocus is trying to focus around that area.
         * This can be used to draw things on screen.
         */
        override fun onFocusStart(point: PointF?) {}

        /**
         * Notifies that a gesture focus event just ended, and the camera converged
         * to a new focus (and possibly exposure and white balance).
         */
        override fun onFocusEnd(successful: Boolean, point: PointF?) {}

        /**
         * Noitifies that a finger gesture just caused the camera zoom
         * to be changed. This can be used, for example, to draw a seek bar.
         */
        override fun onZoomChanged(newValue: Float, bounds: FloatArray?, fingers: Array<PointF>?) {}

        /**
         * Noitifies that a finger gesture just caused the camera exposure correction
         * to be changed. This can be used, for example, to draw a seek bar.
         */
        override fun onExposureCorrectionChanged(newValue: Float, bounds: FloatArray?, fingers: Array<PointF>?) {}

    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater!!.inflate(R.layout.fragment_send_tickle, container, false)
    }

    fun onButtonPressed(uri: Uri) {
        if (mListener != null) {
            mListener!!.onFragmentInteraction(FragmentInteractionListener.FragmentType.SEND)
        }
    }

    override fun onStart() {
        super.onStart()
        cameraView?.start()
        getCameraCallbacks()
        addCameraGestures()
        attach_picture.setOnClickListener { startGallery() }
    }

    override fun onPause() {
        super.onPause()
        cameraView?.stop()
        stopCameraCallbacks()
    }

    override fun onDestroy() {
        super.onDestroy()
        cameraView?.destroy()
    }

    private fun stopCameraCallbacks() {
        cameraView?.removeCameraListener(cameraCallback)

    }

    private fun getCameraCallbacks() {
        cameraView?.addCameraListener(cameraCallback)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is FragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    fun addCameraGestures() {
        cameraView.mapGesture(Gesture.PINCH, GestureAction.ZOOM)
        cameraView.mapGesture(Gesture.TAP, GestureAction.FOCUS_WITH_MARKER)
        cameraView.mapGesture(Gesture.LONG_TAP, GestureAction.CAPTURE)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment SendTickleFragment.
         */

        val LOUVRE_REQUEST_CODE = 10123

        // TODO: Rename and change types and number of parameters
        fun newInstance(): SendTickleFragment {
            return SendTickleFragment()
        }
    }

    fun startGallery() {
        Louvre.init(this@SendTickleFragment)
                .setRequestCode(LOUVRE_REQUEST_CODE)
                .setMaxSelection(1).open()

    }
}// Required empty public constructor
