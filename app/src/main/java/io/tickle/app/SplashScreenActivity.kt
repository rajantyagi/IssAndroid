package io.tickle.app

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.PersistableBundle
import io.tickle.app.base.BaseCompatActivity
import kotlinx.android.synthetic.main.activity_splash_screen.*

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
class SplashScreenActivity : BaseCompatActivity() {
    private val nextScreenHandler = Handler()

    private val mNextScreenRunnable = Runnable {
        val intent = Intent(this@SplashScreenActivity, HomeActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        // Set up the user interaction to manually show or hide the system UI.
        // Upon interacting with UI controls, delay any scheduled hide()
        // operations to prevent the jarring behavior of controls going away
        // while interacting with the UI.
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        moveToNext(2000)
        fullScreenImmersive(fullscreen_content)
    }

    private fun moveToNext(delayMillis: Int) {
        nextScreenHandler.postDelayed(mNextScreenRunnable, delayMillis.toLong())
    }
}
