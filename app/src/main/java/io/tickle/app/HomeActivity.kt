package io.tickle.app

import android.content.ContentValues
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.BottomNavigationView
import android.util.Log
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import android.view.Window
import android.widget.Toast
import io.tickle.app.base.BaseCompatActivity
import io.tickle.app.helpers.FragmentHelpers
import io.tickle.app.helpers.OnSwipeListener
import io.tickle.app.mvp.ui.FragmentInteractionListener
import io.tickle.app.mvp.ui.about.ProfileFragment
import io.tickle.app.mvp.ui.pasttickles.BrowseTickles
import io.tickle.app.mvp.ui.pasttickles.dummy.DummyContent
import io.tickle.app.mvp.ui.tickles.SendTickleFragment
import kotlinx.android.synthetic.main.activity_home.*


class HomeActivity : BaseCompatActivity(), BrowseTickles.OnListFragmentInteractionListener, FragmentInteractionListener, View.OnTouchListener {


    private var gestureDetector: GestureDetector? = null

    private var swipeListener = object : OnSwipeListener() {

        override fun onSwipe(direction: OnSwipeListener.Direction): Boolean {
            if (direction === OnSwipeListener.Direction.left) {
                //do your stuff
                Log.d(ContentValues.TAG, "onSwipe: left")
            }

            if (direction === OnSwipeListener.Direction.right) {
                //do your stuff
                Log.d(ContentValues.TAG, "onSwipe: right")
            }
            return true
        }
    }

    override fun onFragmentInteraction(type: FragmentInteractionListener.FragmentType) {
        Toast.makeText(this@HomeActivity, "clicked", Toast.LENGTH_SHORT).show()
    }

    override fun onListFragmentInteraction(item: DummyContent.DummyItem) {
        Log.e(TAG, "Clicked")
    }

    private val TAG = "HomeActivity"
    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.tickles -> {
                FragmentHelpers.startFragment(this@HomeActivity, BrowseTickles.newInstance(1))
                return@OnNavigationItemSelectedListener true
            }
            R.id.send_tickle -> {
                FragmentHelpers.startFragment(this@HomeActivity, SendTickleFragment.newInstance())
                return@OnNavigationItemSelectedListener true
            }
            R.id.about -> {
                FragmentHelpers.startFragment(this@HomeActivity, ProfileFragment.newInstance())
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    private val handler: Handler? = Handler()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        gestureDetector = GestureDetector(this@HomeActivity, swipeListener)
        container.setOnTouchListener(this@HomeActivity)
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        handler?.postDelayed({
            Log.e(TAG, "delayed")
        }, 1500)
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        fullScreen(container)
    }


    override fun onTouch(p0: View?, p1: MotionEvent?): Boolean {
        Log.d(TAG, "onTouch: ")
        gestureDetector?.onTouchEvent(p1)
        return true
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        return super.onTouchEvent(event)
    }
}
